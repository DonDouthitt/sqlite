using Microsoft.AspNetCore.Hosting;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;

using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;


namespace SQLite.Console
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();

                    try
                    {
                        AddProductUsingAdo("Calipers");
                    }
                    catch (System.Exception)
                    { 
                        
                    }

                    DisplayProductsUsingAdo();


                    try
                    {
                        AddProductUsingEF("3ft. Level");
                    }
                    catch (System.Exception)
                    {

                    }

                    DisplayProductsUsingEF();
                });


        #region ADO.Net
        private static bool AddProductUsingAdo(string ProductName)
        {
            SqliteConnection conn = new SqliteConnection(@"Data Source=DB\ChampionProductsDB.db;");
            conn.Open();

            SqliteCommand command;

            using (conn)
            {
                command = conn.CreateCommand();
                command.CommandText = "Insert into product (Description) values ('" + ProductName + "')";
                command.ExecuteNonQuery();
            }

            return true;
        }
        
        private static void DisplayProductsUsingAdo()
        {
            SqliteConnection conn = new SqliteConnection(@"Data Source=DB\ChampionProductsDB.db;");
            conn.Open();

            SqliteDataReader datareader;
            SqliteCommand command;

            using (conn)
            {
                command = conn.CreateCommand();
                command.CommandText = "Select * from product";

                datareader = command.ExecuteReader();
                while (datareader.Read())
                {
                    string product = datareader.GetString(1);
                    Debug.WriteLine(product);
                }
            }
        }
        #endregion


        #region EF
        public class Product
        {
            [Key]
            public int ProductId { get; set; }
            public string Description { get; set; }
        }

        public class ChampionContext : DbContext
        {
            public DbSet<Product> Products { get; set; }

            protected override void OnConfiguring(DbContextOptionsBuilder options) => options.UseSqlite(@"Data Source=DB\ChampionProductsDB.db;");

            protected override void OnModelCreating(ModelBuilder modelBuilder)
            {
                modelBuilder.Entity<Product>().ToTable("Product");
            }
        }

        private static void AddProductUsingEF(string ProductName)
        {
            using (var db = new ChampionContext())
            {
                var newProduct = new Product()
                {
                    Description = ProductName
                };

                db.Products.Add(newProduct);
                db.SaveChanges();
            }
        }

        private static void DisplayProductsUsingEF()
        {
            using (var db = new ChampionContext()) 
            {
                var products = db.Products.AsNoTracking();

                foreach (var items in products)
                {
                    Debug.WriteLine(items.Description);
                }
            }
        }
        #endregion
    }
}
